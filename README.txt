INTRODUCTION
------------
The Reading Time module adds a field that can be added to any content type that
contains a body field.  The Reading Time field calculates the estimated reading
time for the content contained in the node's body field and displays it. You
have likely seen this functionality on sites like Medium.

Including an estimated reading time provides a better user experience for sites
that make use of longform writing, as people visiting your site will have a more
objective idea of how long it will take them to read the page's content. This
should result in an improvement in user retention on your site.

See http://briancray.com/posts/estimated-reading-time-web-design/ for a
discussion of the benefits of displaying reading time to your site visitors.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
This module has no menu or global settings.  You will be able to configure the
field when adding it to a content type. When enabled, the field will display an
estimated reading time as part of the node's display.

MAINTAINERS
-----------
Maintained by Darren James Harkness - https://www.drupal.org/user/456800/
